from django.apps import AppConfig


class DiagonosticsAppConfig(AppConfig):
    name = 'diagonostics_app'
