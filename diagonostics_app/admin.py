from django.contrib import admin
from .models import Doctor, Appointments, TestType, Test, TestReport, Prescription, HealthTip


class DoctorsAdmin(admin.ModelAdmin):
    list_display = ('Name', 'Designation', 'Email', 'Contact')


class AppointmentsAdmin(admin.ModelAdmin):
    list_display = ('First_Name', 'Last_Name',
                    'Email', 'Doctor_Name', 'Contact')


class TestTypeAdmin(admin.ModelAdmin):
    list_display = ('Type_Name', 'Adding_Time')


class TestAdmin(admin.ModelAdmin):
    list_display = ('Test_Name', 'Test_Type', 'Test_Fee')


class TestReportAdmin(admin.ModelAdmin):
    list_display = ('Patient_Name', 'Patient_Address',
                    'Patient_Email', 'Patient_Contact', 'Total_Fee')


class PrescriptionAdmin(admin.ModelAdmin):
    list_display = ('Patients_Name', 'Doctors_Name',
                    'Prescription', 'Time')


class HealthTipsAdmin(admin.ModelAdmin):
    list_display = ('title', 'description')


# admin.site.register(Doctor)
admin.site.register(Doctor, DoctorsAdmin)
admin.site.register(Appointments, AppointmentsAdmin)
admin.site.register(TestType, TestTypeAdmin)
admin.site.register(Test, TestAdmin)
admin.site.register(TestReport, TestReportAdmin)
admin.site.register(Prescription, PrescriptionAdmin)
admin.site.register(HealthTip, HealthTipsAdmin)
