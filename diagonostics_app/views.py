from django.shortcuts import render
from .models import Doctor, Appointments, Test, HealthTip
from .forms import AppointmentsForm


def Home(request):
    return render(request, 'index.html')


def Doctors(request):
    doctors = Doctor.objects.all()
    doctor_list = {'doctor_list': doctors}
    return render(request, 'doctors.html', doctor_list)


def Appointment(request):
    # Don't make the same funtion n model name..
    appointments = Appointments.objects.all()
    appoint_list = {'appoint_list': appointments}
    return render(request, 'appointments.html', appoint_list)


def Tests(request):
    all_test = Test.objects.all()
    tests = {'tests': all_test}
    return render(request, 'tests.html', tests)


def Health(request):
    all_tips = HealthTip.objects.all()
    tips = {'tips': all_tips}
    return render(request, 'health.html', tips)


def CreateAppointment(request):
    if request.method == 'POST':
        form = AppointmentsForm(request.POST)
        form.save()
    else:
        form = AppointmentsForm
    return render(request, 'createappoint.html', {'form': form})
