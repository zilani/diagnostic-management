from django.urls import path
from diagonostics_app import views

urlpatterns = [

    path('', views.Home),
    path('doctors/', views.Doctors, name="doctors"),
    path('appointments/', views.Appointment, name="appointments"),
    path('tests/', views.Tests, name="tests"),
    path('health-tips/', views.Health, name="health-tips"),
    path('create-appointment/', views.CreateAppointment, name="create-appointment"),
]
