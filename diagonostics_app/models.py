from django.db import models

# admin: diagonostic pass:diagonostic1234


class Doctor(models.Model):
    Name = models.CharField(max_length=30, primary_key=True)
    Designation = models.CharField(max_length=30)
    Email = models.EmailField(max_length=30)
    Contact = models.CharField(max_length=30)

    def __str__(self):
        return self.Name


class Appointments(models.Model):
    First_Name = models.CharField(max_length=30)
    Last_Name = models.CharField(max_length=30)
    Email = models.EmailField(unique=True)
    Doctor_Name = models.ForeignKey(
        Doctor, on_delete=models.CASCADE, max_length=30)
    Contact = models.CharField(max_length=30)

    def __str__(self):
        return "%s Welcome,Your Appointment is Confirmed!" % self.First_Name


class TestType(models.Model):
    Type_Name = models.CharField(primary_key=True, max_length=30)
    Adding_Time = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return "%s :Test Type" % self.Type_Name


class Test(models.Model):
    Test_Name = models.CharField(max_length=30)
    Test_Type = models.ForeignKey(TestType, on_delete=models.CASCADE)
    Test_Fee = models.IntegerField()

    def __str__(self):
        return "%s :Test Name" % self.Test_Name


class TestReport(models.Model):
    Patient_Name = models.CharField(primary_key=True, max_length=30)
    Patient_Address = models.CharField(max_length=30)
    Patient_Email = models.EmailField(max_length=30)
    Patient_Contact = models.CharField(max_length=30)
    Total_Fee = models.IntegerField()

    def __str__(self):
        return "%s :'s Test Report" % self.Patient_Name


class Prescription(models.Model):
    Doctors_Name = models.ForeignKey(Doctor, on_delete=models.CASCADE)
    Patients_Name = models.CharField(max_length=30)
    Prescription = models.TextField()
    Time = models.DateTimeField()

    def __str__(self):
        return "%s :'s Prescription" % self.Patients_Name


class HealthTip(models.Model):
    title = models.CharField(max_length=30)
    description = models.TextField()

    def __str__(self):
        return self.title
