from django.contrib import admin
from django.urls import path
from user_auth import views
from django.contrib import auth

urlpatterns = [
    path('login/', views.user_login, name='login'),
    path('register/', views.register, name='register'),
    path('success/', views.success, name='success'),
    path('', views.home, name='home'),
    path('logout/', views.user_logout, name='logout'),
    path('dashboard/', views.dashboard, name='dashboard'),
    path('dashboard/profile/', views.user_profile, name='profile'),
    path('dashboard/users/', views.users, name='users'),

]
