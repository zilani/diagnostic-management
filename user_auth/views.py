from django.shortcuts import render, redirect
from django.http import HttpResponse, HttpResponseRedirect
from django.contrib.auth import authenticate, login, logout
from .forms import LoginForm, UserRegistrationForm
from django.contrib.auth.decorators import login_required
from django.contrib.auth.models import User
# from django.contrib import messages

def user_login(request):
    if request.method == 'POST':
        form = LoginForm(request.POST)
        if form.is_valid():
            cd = form.cleaned_data
            user = authenticate(username=cd['username'],
                                password=cd['password'])
            if user is not None:
                if user.is_active:
                    login(request, user)
                    return HttpResponseRedirect('/dashboard/')
                else:
                    return HttpResponse("Authentication Failed")
    else:
        form = LoginForm()
    return render(request, 'auth/login.html', {'form': form})


def user_logout(request):
    logout(request)
    return HttpResponseRedirect('/login/')


def register(request):
    if request.method == 'POST':
        user_form = UserRegistrationForm(request.POST)
        if user_form.is_valid():
            new_user = user_form.save(commit=False)
            new_user.set_password(
                user_form.cleaned_data['password'])
            new_user.save()

            return render(request,
                      'auth/success.html',
                      {'new_user': new_user})

    else:
        user_form = UserRegistrationForm()
    return render(request,
                  'auth/user_register.html',
                  {'user_form': user_form})


def success(request):
    return render(request, 'auth/success.html')


def home(request):
    return render(request, 'auth/index.html')


@login_required
def dashboard(request):
    return render(request,
                  'auth/dashboard.html',
                  {'section': 'dashboard'})

@login_required
def user_profile(request):
    return render(request, 'auth/user_profile.html')

@login_required
def users(request):
    all_user = User.objects.filter(is_staff=False)
    user = {'user': all_user}
    return render(request, 'auth/users.html', user)